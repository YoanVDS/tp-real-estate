﻿namespace Bowling_Library
{
    public class BowlingGame
    {
        public int RemainingPins = 10;
        
        private int _currentShot = 1;

        public int CurrentRound = 1;
        public int CurrentScore = 0;

        private int _currentPlayer = 1;
        public int Player1Score = 0;
        public int Player2Score = 0;

        public BowlingGame() { }

        public void Hit(int pinsCount)
        {
            if (pinsCount > RemainingPins || pinsCount < 0) throw new ArgumentException("Wrong number of pins!");
            RemainingPins -= pinsCount;
            CurrentScore += pinsCount;
            if (_currentShot == 2)
            {
                processPoints();
                nextRound();
            }
            else _currentShot = 2;
        }

        private void nextRound()
        {
            CurrentScore = 0;
            _currentShot = 1;
            RemainingPins = 10;
            CurrentRound++;
        }

        private void processPoints()
        {
            if (_currentPlayer == 1)
            {
                Player1Score += CurrentScore;
                _currentPlayer = 2;
            }
            else
            {
                Player2Score += CurrentScore;
                _currentPlayer = 1;
            }
        }
    }
}