using Xunit;
using Bowling_Library;
using System;

namespace Bowling_Tests
{
    public class UnitTest1
    {
        BowlingGame game = new BowlingGame();

        [Theory(DisplayName = "Hit")]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        [InlineData(6)]
        [InlineData(7)]
        [InlineData(8)]
        [InlineData(9)]
        [InlineData(10)]
        public void TestHit(int pinsCount)
        {
            game.Hit(pinsCount);
            Assert.Equal(10 - pinsCount, game.RemainingPins);
        }

        [Theory(DisplayName = "Hit Exception 1")]
        [InlineData(-1)]
        [InlineData(11)]
        public void TestHitException1(int pinsCount)
        {
            Assert.Throws<ArgumentException>(() => game.Hit(pinsCount));
        }

        [Theory(DisplayName = "Hit Exception 2")]
        [InlineData(5,6)]
        [InlineData(7,4)]
        public void TestHitException2(int pinsCount1, int pinsCount2)
        {
            game.Hit(pinsCount1);
            Assert.Throws<ArgumentException>(() => game.Hit(pinsCount2));
        }

        [Theory(DisplayName = "Player Points")]
        [InlineData(1, 6, 3, 4)]
        [InlineData(2, 6, 2, 5)]
        public void TestPlayerPoints(int p1Pins1, int p1Pins2, int p2Pins1, int p2Pins2)
        {
            game.Hit(p1Pins1);
            game.Hit(p1Pins2);

            game.Hit(p2Pins1);
            game.Hit(p2Pins2);

            Assert.Equal(p1Pins1 + p1Pins2, game.Player1Score);
            Assert.Equal(p2Pins1 + p2Pins2, game.Player2Score);
        }
    }
}