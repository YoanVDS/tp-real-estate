﻿namespace FizzBuzz_Kata_Library
{
    public class FizzBuzzGenerator
    {
        public static string GenerateFizzBuzzString(int count)
        {
            if (count < 15 || count > 150) throw new ArgumentOutOfRangeException("The count should be comprised between 15 and 150!");
            string result = "";
            for(int i = 1; i <= count; i++)
            {
                if (i % 3 == 0 && i % 5 == 0) result += "FizzBuzz";
                else if (i % 3 == 0) result += "Fizz";
                else if (i % 5 == 0) result += "Buzz";
                else result += i;
                result += " ";
            }
            return result;
        }
    }
}