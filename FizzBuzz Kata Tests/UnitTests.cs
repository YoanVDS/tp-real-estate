using Xunit;
using FizzBuzz_Kata_Library;
using System;

namespace FizzBuzz_Kata_Tests
{
    public class UnitTests
    {
        [Theory(DisplayName = "Generate")]
        [InlineData(15,"1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz ")]
        [InlineData(20,"1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz 16 17 Fizz 19 Buzz ")]
        public void TestGenerate(int count, string expected)
        {
            Assert.Equal(expected, FizzBuzzGenerator.GenerateFizzBuzzString(count));
        }

        [Theory(DisplayName ="Generate Exceptions")]
        [InlineData(14)]
        [InlineData(151)]
        public void TestGenerateExceptions(int count)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => FizzBuzzGenerator.GenerateFizzBuzzString(count));
        }
    }
}