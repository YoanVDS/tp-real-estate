﻿using System;
using Real_Estate_Library;

namespace Real_Estate_App
{
    class Program
    {
        static void Main(string[] args)
        {
            HealthInfo itEngineerInfo = new HealthInfo(true, false, true, true, false);
            Credit itEngineerCredit = new Credit(175000, 25, NominalRate.GOOD_RATE, itEngineerInfo);

            HealthInfo chaserInfo = new HealthInfo(false, true, false, false, true);
            Credit chaserCredit = new Credit(200000, 15, NominalRate.VERY_GOOD_RATE, chaserInfo);

            Console.WriteLine("IT Engineer's Credit: ");
            Console.WriteLine("---------------------");
            Console.WriteLine(itEngineerCredit.ToString());
            Console.WriteLine("---------------------");
            Console.WriteLine("");

            Console.WriteLine("Chaser's Credit: ");
            Console.WriteLine("---------------------");
            Console.WriteLine(chaserCredit.ToString());
            Console.WriteLine("---------------------");
            Console.WriteLine("");
        }
    }
}
