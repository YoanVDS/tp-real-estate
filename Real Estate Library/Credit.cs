﻿using System;

namespace Real_Estate_Library
{
	public class Credit
	{
		private Capital _capital;
		private Duration _duration;
		private HealthInfo _healthInfo;
		private NominalRate _rate;
		private Insurance _insurance;
		public double MonthlyPayment;
		public double MonthlyInsuranceCost;


		public Credit(float capital, int durationInYears, int rateType, HealthInfo healthInfo)
		{
			_capital = new Capital(capital);
			_duration = new Duration(durationInYears);
			_healthInfo = healthInfo;
			_rate = new NominalRate(durationInYears, rateType);
			_insurance = new Insurance(_healthInfo);

			MonthlyInsuranceCost = (_insurance.ToPercentage() * _capital.Value) / 12;
			MonthlyPayment = (_capital.Value * (_rate.ToPercentage() / 12))
							/ (1 - Math.Pow(1 + _rate.ToPercentage() / 12, - _duration.InMonths())) + MonthlyInsuranceCost;
		}

		public double Capital() => _capital.Value;
		public int DurationInYears() => _duration.Value;
		public int DurationInMonths() => _duration.InMonths();
		public double Rate() => _rate.Value;
		public double Insurance() => _insurance.Value;
		public double TotalInterests() => MonthlyPayment * DurationInMonths() - Capital();
		public double TotalInsuranceCost() => MonthlyInsuranceCost * DurationInMonths();
		public double PaidCapitalInTenYears() => MonthlyPayment * 120;

		private string showCapital() => "Capital: " + Capital() + "$." + Environment.NewLine;
		private string showDuration() => "Credit duration: " + DurationInYears() + " years." + Environment.NewLine;
		private string showRate() => "Credit rate: " + Rate() + "%." + Environment.NewLine;
		private string showInsurance() => "Insurance rate: " + Insurance() + "%." + Environment.NewLine;
		private string showMonthlyPayment() => "Monthly paiement: " + Math.Round(MonthlyPayment,2) + "$." + System.Environment.NewLine;
		private string showMonthlyInsuranceCost() => "Monthly insurance cost: " + Math.Round(MonthlyInsuranceCost,2) + "$." + System.Environment.NewLine;
		private string showTotalInterests() => "Total interests: " + Math.Round(TotalInterests(),2) + "$." + System.Environment.NewLine;
		private string showTotalInsuranceCost() => "Total insurance cost: " + Math.Round(TotalInsuranceCost(),2) + "$." + System.Environment.NewLine;
		private string showPaidCapitalInTenYears() => "Paid capital in 10 years: " + Math.Round(PaidCapitalInTenYears(), 2) + "$." + System.Environment.NewLine;

		public override string ToString()
		{
			return showCapital() 
				 + showDuration() 
				 + showRate()
				 + showInsurance()
				 + showMonthlyPayment() 
				 + showMonthlyInsuranceCost()
				 + showTotalInterests() 
				 + showTotalInsuranceCost() 
				 + showPaidCapitalInTenYears();
		}


	}
}