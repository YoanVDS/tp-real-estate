﻿using System;

namespace Real_Estate_Library
{
	public class Duration
	{
		public int Value;

		public Duration(int value)
		{
			Value = value;
		}

		public int InMonths() => Value * 12;
	}
}