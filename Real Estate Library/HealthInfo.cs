﻿using System;

namespace Real_Estate_Library
{
	public class HealthInfo
	{
		public bool IsSmoker;
		public bool IsSporty;
		public bool IsCardiac;
		public bool IsITEngineer;
		public bool IsFighterPilot;

		public HealthInfo(bool isSmoker, bool isSporty, bool isCardiac, bool isITEngineer, bool isFighterPilot)
		{
			IsSmoker = isSmoker;
			IsSporty = isSporty;
			IsCardiac = isCardiac;
			IsITEngineer = isITEngineer;
			IsFighterPilot = isFighterPilot;
		}
	}
}