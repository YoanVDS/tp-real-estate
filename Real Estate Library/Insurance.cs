﻿using System;

namespace Real_Estate_Library
{
	public class Insurance
	{
		public double Value = 0.3;

		public Insurance(HealthInfo healthInfo)
		{
			if (healthInfo.IsSporty) Value -= 0.05;
			if (healthInfo.IsSmoker) Value += 0.15;
			if (healthInfo.IsCardiac) Value += 0.3;
			if (healthInfo.IsITEngineer) Value -= 0.05;
			if (healthInfo.IsFighterPilot) Value += 0.15;
		}

		public double ToPercentage() => Value / 100;
	}
}