﻿using System;
using System.Collections.Generic;

namespace Real_Estate_Library
{
	public class NominalRate
	{
		public static int GOOD_RATE = 0;
		public static int VERY_GOOD_RATE = 1;
		public static int EXCELLENT_RATE = 2;

		private static Dictionary<int, double> _goodRates;
		private static Dictionary<int, double> _veryGoodRates;
		private static Dictionary<int, double> _excellentRates;

		private static Dictionary<int, Dictionary<int, double>> _rates;

		static NominalRate()
		{
			_goodRates = new Dictionary<int, double>(){
			{ 7, 0.62 },
			{ 10, 0.67},
			{ 15, 0.85},
			{ 20, 1.04},
			{ 25, 1.27}
		};
			_veryGoodRates = new Dictionary<int, double>(){
			{ 7, 0.43 },
			{ 10, 0.55},
			{ 15, 0.73},
			{ 20, 0.91},
			{ 25, 1.15}
		};
			_excellentRates = new Dictionary<int, double>(){
			{ 7, 0.35 },
			{ 10, 0.45},
			{ 15, 0.58},
			{ 20, 0.73},
			{ 25, 0.89}
		};

			_rates = new Dictionary<int, Dictionary<int, double>>()
		{
			{ GOOD_RATE, _goodRates },
			{ VERY_GOOD_RATE, _veryGoodRates },
			{ EXCELLENT_RATE, _excellentRates}
		};
		}
		public double Value;

		public NominalRate(int years, int rateType)
		{
			Value = _rates[rateType][years];
		}

		public double ToPercentage() => Value / 100;
	}
}