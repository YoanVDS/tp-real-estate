using System;
using Xunit;
using Real_Estate_Library;

namespace Real_Estate_Tests
{
    public class CapitalUnitTests
    {
        [Fact]
        public void CapitalValueTest()
        {
            Capital cap = new Capital(150000);
            Assert.Equal(150000, cap.Value);
        }
    }
}
