﻿using System;
using Xunit;
using Real_Estate_Library;

namespace Real_Estate_Tests
{
    public class CreditUnitTests
    {
        Credit credit = new Credit(175000,
                                   25,
                                   NominalRate.GOOD_RATE,
                                   new HealthInfo(true, false, true, true, false)
                                  );
        [Fact]
        public void CapitalTest()
        {
            Assert.Equal(175000, credit.Capital());
        }

        [Fact]
        public void DurationInYearsTest()
        {
            Assert.Equal(25, credit.DurationInYears());
        }

        [Fact]
        public void DurationInMonthsTest()
        {
            Assert.Equal(25 * 12, credit.DurationInMonths());
        }

        [Fact]
        public void RateTest()
        {
            Assert.Equal(1.27, credit.Rate());
        }

        [Fact]
        public void InsuranceTest()
        {
            Assert.Equal(0.7, credit.Insurance());
        }

        [Fact]
        public void MonthlyPaymentTest()
        {
            Assert.Equal(783.22, Math.Round(credit.MonthlyPayment,2));
        }

        [Fact]
        public void MonthlyInsuranceCostTest()
        {
            Assert.Equal(102.08, Math.Round(credit.MonthlyInsuranceCost, 2));
        }

        [Fact]
        public void TotalInterestsTest()
        {
            Assert.Equal(59965.69, Math.Round(credit.TotalInterests(), 2));
        }

        [Fact]
        public void TotalInsuranceCostTest()
        {
            Assert.Equal(30625, Math.Round(credit.TotalInsuranceCost(), 2));
        }

        [Fact]
        public void PaidCapitalInTenYearsTest()
        {
            Assert.Equal(93986.28, Math.Round(credit.PaidCapitalInTenYears(), 2));
        }
    }
}
