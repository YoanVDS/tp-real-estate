﻿using System;
using Xunit;
using Real_Estate_Library;

namespace Real_Estate_Tests
{
    public class DurationUnitTests
    {
        Duration duration = new Duration(10);

        [Fact]
        public void DurationValueTest()
        {
            Assert.Equal(10, duration.Value);
        }

        [Fact]
        public void DurationInMonthsTest()
        {
            Assert.Equal(10 * 12, duration.InMonths());
            Assert.Equal(12 * duration.Value, duration.InMonths());
        }
    }
}
