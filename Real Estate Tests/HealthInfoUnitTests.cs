﻿using System;
using Xunit;
using Real_Estate_Library;

namespace Real_Estate_Tests
{
    public class HealthInfoUnitTests
    {
        HealthInfo info = new HealthInfo(true, false, false, true, true);

        [Fact]
        public void SmokerTest()
        {
            Assert.True(info.IsSmoker);
        }

        [Fact]
        public void SportyTest()
        {
            Assert.False(info.IsSporty);
        }

        [Fact]
        public void CardiacTest()
        {
            Assert.False(info.IsCardiac);
        }

        [Fact]
        public void ITEngineerTest()
        {
            Assert.True(info.IsITEngineer);
        }

        [Fact]
        public void FighterPilotTest()
        {
            Assert.True(info.IsFighterPilot);
        }
    }
}
