﻿using System;
using Xunit;
using Real_Estate_Library;

namespace Real_Estate_Tests
{
    public class InsuranceUnitTests
    {
        Insurance insurance = new Insurance(
                                   new HealthInfo(true, false, true, false, true)
                                            );
        
        [Fact]
        public void ValueTest()
        {
            //Base: 0.3
            //Smoker: + 0.15
            //Cardiac: + 0.3
            //Fighter Pilot: + 0.15
            //We assume 0.9
            Assert.Equal(0.9, insurance.Value);
        }

        [Fact]
        public void ToPercentageTest()
        {
            Assert.Equal(0.009, Math.Round(insurance.ToPercentage(),3));
        }
    }
}
