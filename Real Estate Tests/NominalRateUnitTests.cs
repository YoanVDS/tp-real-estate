﻿using System;
using Xunit;
using Real_Estate_Library;

namespace Real_Estate_Tests
{
    public class NominalRateUnitTests
    {
        NominalRate rate = new NominalRate(10, NominalRate.GOOD_RATE);
        
        [Fact]
        public void ValueTest()
        {
            Assert.Equal(0.67,rate.Value);
        }
    }
}
