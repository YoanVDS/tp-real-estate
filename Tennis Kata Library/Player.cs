﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tennis_Kata_Library
{
    public class Player
    {
        static readonly int[] _scoresList = { 0, 15, 30, 40, 40 };
        public string Name;

        private int _score = 0;

        public Player(string name)
        {
            Name = name;
        }

        public void Scores()
        {
            if (HasMaximumPoints()) throw new ArgumentException("The player has already the maximum of points!");
            _score++;
        }

        public int Score() => _scoresList[_score];

        public bool HasMaximumPoints() => _score == 4;
    }
}
