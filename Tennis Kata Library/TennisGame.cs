﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tennis_Kata_Library
{
    public class TennisGame
    {
        public Player Player1;
        public Player Player2;
        public Player FormerScorer;
        public Player Scorer;

        public TennisGame(Player player1, Player player2)
        {
            Player1 = player1;
            Player2 = player2;
        }

        public void Scores(Player player)
        {
            checkPlayerInGame(player);
            if(!player.HasMaximumPoints()) player.Scores();
            FormerScorer = Scorer;
            Scorer = player;
        }

        private bool checkPlayerInGame(Player player) => isInGame(player) ? true
                                                                          : throw new ArgumentException("The player isn't registered in this game.");

        private bool isInGame(Player player) => (player == Player1 || player == Player2);

        public int ScoreOf(Player player) => checkPlayerInGame(player) ? player.Score()
                                                                       : -1;

        public Player WinningPlayer()
        {
            if (equality() && scorerTwice()) return Scorer;
            else if (!equality() && player1Won()) return Player1;
            else if (!equality() && player2Won()) return Player2;
            else return null;
        }

        private bool equality() => Player1.Score() == 40 && Player2.Score() == 40;
        private bool scorerTwice() => FormerScorer == Scorer;
        private bool player1Won() => Player1.HasMaximumPoints();
        private bool player2Won() => Player2.HasMaximumPoints();
    }
}
