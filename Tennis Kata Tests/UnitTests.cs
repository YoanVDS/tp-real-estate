using System;
using Xunit;
using Tennis_Kata_Library;

namespace Tennis_Kata_Tests
{
    public class PlayerTests
    {
        Player player = new Player("John");

        [Fact(DisplayName = "Name")]
        public void NameTest()
        {
            Assert.Equal("John", player.Name);
        }

        [Fact(DisplayName = "Score")]
        public void ScoreTest()
        {
            Assert.Equal(0, player.Score());
            player.Scores();
            Assert.Equal(15, player.Score());
            player.Scores();
            Assert.Equal(30, player.Score());
            player.Scores();
            Assert.Equal(40, player.Score());
            player.Scores();
            Assert.Equal(40, player.Score());
        }

        [Fact(DisplayName = "Score Exception")]
        public void ScoreExceptionTest()
        {
            for (int i = 0; i < 4; i++) player.Scores();
            Assert.Throws<ArgumentException>(() => player.Scores());
        }

        [Fact(DisplayName = "Has Max Points")]
        public void HasMaxPointsTest()
        {
            Assert.False(player.HasMaximumPoints());
            for (int i = 0; i < 4; i++) player.Scores();
            Assert.True(player.HasMaximumPoints());
        }
    }

    public class GameTests
    {
        Player john;
        Player marie;
        TennisGame game;

        public GameTests()
        {
            john = new Player("John");
            marie = new Player("Marie");
            game = new TennisGame(john, marie);
        }

        [Fact(DisplayName = "Players affectation")]
        public void PlayersAffectationTest()
        {
            Assert.Equal(john, game.Player1);
            Assert.Equal(marie, game.Player2);
        }

        [Fact(DisplayName = "Player Scores")]
        public void PlayerScoresTest()
        {
            game.Scores(john);
            Assert.Equal(15, game.ScoreOf(john));
        }

        [Fact(DisplayName = "Scorer")]
        public void ScorerTest()
        {
            game.Scores(john);
            game.Scores(marie);
            Assert.Equal(marie, game.Scorer);
        }

        [Fact(DisplayName = "Former Scorer")]
        public void FormerScorerTest()
        {
            game.Scores(john);
            game.Scores(marie);
            Assert.Equal(john, game.FormerScorer);
        }

        [Fact(DisplayName = "Player not in game Exception")]
        public void PlayerNotInGameExceptTest()
        {
            Player benji = new Player("Benji");
            Assert.Throws<ArgumentException>(() => game.Scores(benji));
            Assert.Throws<ArgumentException>(() => game.ScoreOf(benji));
        }

        [Fact(DisplayName = "Player Won")]
        public void PlayerWonTest()
        {
            for (int i = 0; i < 4; i++) game.Scores(marie);
            Assert.Equal(marie, game.WinningPlayer());
        }

        [Fact(DisplayName = "Player Won - After Equality")]
        public void PlayerWonAfterEqualityTest()
        {
            for (int i = 0; i < 3; i++) game.Scores(john);
            for (int i = 0; i < 3; i++) game.Scores(marie);
            game.Scores(john);
            Assert.Null(game.WinningPlayer());
            game.Scores(john);
            Assert.Equal(john, game.WinningPlayer());
        }
    }
}
